const _ = {}

// Basic parts
{
  // Three nadis and chakras of both roles
  {
    _.pingaraNadis = () => `Pingara nadis`
    _.idaNadis = () => `Ida nadis`
    _.sushumnaNadis = () => `Sushumna nadis`

    _.absorbingRoleChakras = () => `absorbing-role chakras`
    _.releasingRoleChakras = () => `releasing-role chakras`
    _.chakrasOfRole = (role) =>
      (role === 'releasing') ? _.releasingRoleChakras() : _.absorbingRoleChakras()

    _.nadisAndChakrasArray = () => [
      _.pingaraNadis(),
      _.idaNadis(),
      _.sushumnaNadis(),
      _.releasingRoleChakras(),
      _.absorbingRoleChakras(),
    ]
  }

  // Bodies
  {
    _.fiveBodies = () =>
      `physical, etheric, astral, mental, and causal bodies`

    _.fourBodies = () =>
      `physical, etheric, astral, and mental bodies`
  }

  // Chakras
  {
    _.allMajorAndMinorChakras = () =>
      `all major chakras and all minor chakras ${_.in4SystemsOfChakra()}`

    _.allMajor9AndMinor42Chakras = () =>
      `9 major chakras -- Muladhara, Swadhisthana, Manipura, Surya, Manas, Anahata, Vishuddha, Ajna, and Sahasrara -- and 42 minor chakras ${_.in4SystemsOfChakra()}`

    _.chakrasAbsorbing = () => `absorbing-role chakras`

    _.fourChakraRoutes = () =>
      `out-of-the-body, body surface, nerve plexus, and spinal cord`

    _.in4SystemsOfChakra = () =>
      `in the 4 systems -- ${_.fourChakraRoutes()} --`

    // TODO Replace the routes with _.fourChakraRoutes()
    _.inFourChakraRoutes = () =>
      `in the upper surface, body surface, nerve plexus, and spinal cord`
  }

  // Other things
  {
    _.excessDoshaAndAma = () =>
      `excess Vata, excess Pitta, excess Kapha, negative prana, and Ama`

    _.allDeitiesAndSoOn12Systems = () =>
      `all deities and their wraiths, all people and their wraiths, all divine spirits and their wraiths, all spirits and their wraiths, and all evil spirits and their wraiths ${_.in12Systems()}`

    _.among12Systems = () =>
      `among the 12 Systems`

    _.everyNegatives = () =>
      `every ${_.negatives()}`

    _.in12Systems = () =>
      `in the 12 Systems`

    _.interactingWith = (my = 'my') =>
      `interacting with ${my} entities in any of the 7 sub-sub-sublokas of the 7 sub-sublokas of the 7 sublokas of the 7 lokas among the 12 Systems in the conscious, subconscious and unconscious realms`

    _.mayYouGrant = (forNext = '') =>
      `May You grant all these prayers ${forNext}, as far as Karma permits.`

    _.motherDivine12Systems = () =>
      `All Mother Divines ${_.in12Systems()}`

    _.negatives = () =>
      `negative energy, force, thought, impression, emotion, and prana`

    _.repeatGayatriMantra = ({withShanti}) => {
      const andEndItWith = withShanti ? `, and end it with "Om, Shanti, Shanti, Shanti-hi"` : ``
      return `/* Repeat Gayatri Mantra 3 times${andEndItWith}. */`
    }
  }
}

// Common preface
{
  _.removeFromOnPreface = (element) =>
    `${_.motherDivine12Systems()}, Please remove ${_.everyNegatives()} from all my ${element} ${_.among12Systems()}.`

  _.forgive = () =>
    `${_.motherDivine12Systems()}, I have committed so many sins throughout my life.
I sincerely chant Gayatri Mantra so that all these sins may be forgiven.`

  _.prefaceParagraphs = () =>
    [
      ..._.nadisAndChakrasArray().map((element) => _.removeFromOnPreface(element)),
      `${_.motherDivine12Systems()}, \
        all my hearts ${_.among12Systems()} are veiled with darkness. \
        May this darkness be taken away and may all my hearts be filled with inner brilliant light.`,
      `${_.motherDivine12Systems()}, `,
    ]
}

// Prayer for Exorcism (12 Systems)
{
  // Please remove every aggressions ...
  {
    _.pleaseRemoveAggressions = () =>
      `Please remove every aggressions and ${_.everyNegatives()}`

    _.removeAggressionsFrom = () =>
      `${_.pleaseRemoveAggressions()} being given by ${_.allDeitiesAndSoOn12Systems()}
        from my ${_.fourBodies()}
        ${_.interactingWith()}.`

    _.removeAggressionsWhichHaveSuffered = () =>
      `${_.pleaseRemoveAggressions()} of ${_.allDeitiesAndSoOn12Systems()}
        which my ${_.fourBodies()}
        ${_.interactingWith()}
        have suffered.`

    _.returnToTheirOrigin = () =>
      `Please return continuously to their origins 
all these removed aggressions and
all these removed negative energy, force, thought, impression, emotion, and prana
${_.among12Systems()}.`
  }

  // Please eliminate ...
  {
    _.pleaseEliminate = (ofWhat) =>
      `Please eliminate
      ${_.allDeitiesAndSoOn12Systems()}
      which have possessed any of ${ofWhat}
      ${_.interactingWith()}.`

    _.eliminateParagraphs = () => [
      _.pleaseEliminate(`my ${_.fourBodies()}`),
      _.pleaseEliminate(_.allMajorAndMinorChakras()),
      _.pleaseEliminate(_.allMajor9AndMinor42Chakras()),
    ]
  }

  // Please prevent
  {
    _.myBodiesAndChakras = () =>
      `my* ${_.fourBodies()} ${_.interactingWith('my*')};
      ${_.allMajorAndMinorChakras()} ${_.interactingWith('my*')};
      and ${_.allMajor9AndMinor42Chakras()} ${_.interactingWith('my*')}`

    _.pleasePreventAgain = () =>
      `Please prevent
      ${_.allDeitiesAndSoOn12Systems()}
      which have been removed from possessing once again any of
      ${_.myBodiesAndChakras()}
      and please prevent
      my ${_.negatives()} from making them return,
      then possess me once again or attach anew.`

    _.pleasePreventAnew = () =>
      `Please prevent
      ${_.allDeitiesAndSoOn12Systems()}
      from possessing anew any of 
      ${_.myBodiesAndChakras()}.`
  }

  // Entire the prayer.
  {
    _.exorcismPrayerParagraphs = () =>
      [
        _.forgive(),
        _.repeatGayatriMantra({withShanti: false}),
        ..._.prefaceParagraphs(),
        _.removeAggressionsFrom(),
        _.removeAggressionsWhichHaveSuffered(),
        _.returnToTheirOrigin(),
        ..._.eliminateParagraphs(),
        _.pleasePreventAgain(),
        _.pleasePreventAnew(),
        _.mayYouGrant('for the next 24 consecutive hours'),
        _.repeatGayatriMantra({withShanti: true}),
      ]
  }
}

// Prayer for Purification (12 Systems)
{
  // Please remove ...
  {
    _.allSrotases = () => `all Srotases`
    _.allNadis = ({where}) => `all nadis ${where}`
    _.allRightNadis = () => _.allNadis({where: `in the right(left) side`})
    _.allLeftNadis = () => _.allNadis({where: `in the left(right) side`})
    _.allMidlineNadis = () => _.allNadis({where: `along the midline`})
    _.allChakrasToPurify = ({role, seven}) => {
      const theSeven = seven ? `the 7 ` : ``
      return `all ${theSeven}${_.chakrasOfRole(role)}`
    }

    _.pleaseRemove = (what, from) =>
      `Please remove ${what}
        from ${from}
        ${_.interactingWith()}.`

    _.pleaseRemoveParagraphs = () => [
      _.pleaseRemove(`all ${_.excessDoshaAndAma()}`, `${_.allSrotases()} in my ${_.fourBodies()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.allRightNadis()} of my ${_.fourBodies()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.allLeftNadis()} of my ${_.fourBodies()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.allMidlineNadis()} of my ${_.fourBodies()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.pingaraNadis()} in my ${_.fourBodies()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.idaNadis()} in my ${_.fourBodies()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.sushumnaNadis()} in my ${_.fourBodies()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.allChakrasToPurify({role: 'releasing'})} ${_.inFourChakraRoutes()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.allChakrasToPurify({role: 'absorbing'})} ${_.inFourChakraRoutes()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.allChakrasToPurify({
        role: 'releasing',
        seven: true
      })} ${_.inFourChakraRoutes()}`),
      _.pleaseRemove(_.everyNegatives(), `${_.allChakrasToPurify({
        role: 'absorbing',
        seven: true
      })} ${_.inFourChakraRoutes()}`),
    ]
  }

  // Please prevent ... from intruding once again ...
  {
    _.pleasePreventFromIntruding = () =>
      `Please prevent all these removed ${_.excessDoshaAndAma()}
        and all these removed ${_.negatives()}
        from intruding once again into any of
        ${_.allSrotases()} in my* ${_.fourBodies()} ${_.interactingWith('my*')};
        ${_.allRightNadis()}, ${_.allLeftNadis()}, ${_.allMidlineNadis()}, \
          ${_.pingaraNadis()}, ${_.idaNadis()}, and ${_.sushumnaNadis()} \
          in my* ${_.fourBodies()} ${_.interactingWith('my*')};
        and ${_.allChakrasToPurify({role: 'releasing'})}, \
          ${_.allChakrasToPurify({role: 'absorbing'})}, \
          ${_.allChakrasToPurify({role: 'releasing', seven: true})}, \
          and ${_.allChakrasToPurify({role: 'absorbing', seven: true})} \
          ${_.inFourChakraRoutes()} ${_.interactingWith('my*')}.
        `
  }

  // Please raise the vibration of ...
  {
    _.pleaseRaise = (what) =>
      `Please raise the vibration of ${what}
        ${_.interactingWith()}
        up to the level of Sahasrara Chakra.`

    _.pleaseRaiseParagraphs = () => [
      _.pleaseRaise(`the hearts`),
      _.pleaseRaise(`the bodies`),
      _.pleaseRaise(`the chakras ${_.inFourChakraRoutes()}`),
      _.pleaseRaise(`my ${_.fiveBodies()}`),
    ]
  }

  // Entire the prayer.
  {
    _.purificationPrayerParagraphs = () =>
      [
        ..._.prefaceParagraphs(),
        ..._.pleaseRemoveParagraphs(),
        _.pleasePreventFromIntruding(),
        ..._.pleaseRaiseParagraphs(),
        _.mayYouGrant(),
        _.repeatGayatriMantra({withShanti: true}),
      ]
  }
}

// Entire the document
{
  _.prayerText = () => formatParagraphs([
    `## The Prayers with Gayatri Mantra - Prayer for Exorcism (12 Systems)`,
    ..._.exorcismPrayerParagraphs(),
    `## The Prayers with Gayatri Mantra - Prayer for Purification (12 Systems)`,
    ..._.purificationPrayerParagraphs(),
  ])
}

// Utilities
{
  function formatParagraphs(paragraphs) {
    return paragraphs
      .map((para) => para
        .trim()
        // Remove continuous multiple spaces
        .replace(/ {2,}/g, ' ')
        // Remove extra spaces preceding the comma
        .replace(/ ,/g, ',')
        // Remove indenting spaces
        .replace(/^ +/gm, '')
        // Remove continuous multiple newlines
        .replace(/\n{2,}/gm, '\n')
      ).join('\n\n')
  }
}

module.exports = _

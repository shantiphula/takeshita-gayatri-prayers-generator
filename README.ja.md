# 「除霊と浄化の祈り」

竹下雅敏氏により開発された「除霊と浄化の祈り」の[英語版](https://shanti-phula.net/intl/mg/purify-gayatri-prayers/)のジェネレータです。

## ジェネレータの目的

この祈り文は大変複雑で、同じような表現が何度も繰り返し現れます。
祈り文の英訳版を管理する上で、このジェネレータを使うことで次のメリットがあります。

- 文章の構造を明確にする。
- ある単語の訂正を、何箇所にもわたって行う必要がないので、訂正の労力を減らせる。
- 誤りがなく、一貫性のある英訳文を出力できる。
- Gitにより変更履歴を管理できる。

## 使用方法

次のようにして、最終の祈り文テキスト `output.txt` を得ます。GitクライアントとNode.jsが必要です。

```
git clone git@gitlab.com:shantiphula/takeshita-gayatri-prayers-generator.git

cd takeshita-gayatri-prayers-generator

node print-prayer.js > output.txt
```

## 改訂方法

`prayer.js` を編集します。

`_` オブジェクトに、祈り文を構成する各要素を関数として定義しています。
これらの関数には次の４タイプがあります。

1. 段落群(`Array<String>`)を返すもの
  - 関数名は _.xxxxParagraphs という形式
2. その他配列(`Array<String>`)を返すもの
  - 関数名は _.xxxxArray という形式
3. 最終的な祈り文テキスト(`String`)を返すもの
   - 最下部の `_.prayerText`
4. 文字列(`String`)を返すもの
  - その他の関数はすべてこれに該当

## 著作権

日本語原文の著作権は竹下雅敏氏が保有しています。
